blueprint:
  name: Thermostat Control Automation
  description: "
  This automation sets the desired temperature based on the time of day, window state, and occupancy. 
  It also allows manual control for a set duration. 
  Manual control can be activated by setting the temperature on the thermostat. 
  Manual mode can be turned off my setting the thermostat to 5.0°C."
  domain: automation
  input:
    thermostat_entity:
      name: Thermostat Entity
      description: The entity that controls the thermostat.
      selector:
        entity:
          domain:
            - climate
    desired_temperature_entity:
      name: Desired Temperature Entity
      description: The entity that holds the desired temperature.
      selector:
        entity:
          domain:
            - input_number
    actual_temperature_entity:
      name: Actual Temperature Entity
      description: The entity that holds the actual temperature.
      selector:
        entity:
          domain:
            - sensor
            - input_number
          multiple: false
    manual_control_duration:
      name: Manual Control Duration
      description: The duration in minutes to keep the manual control active.
      default: 30
      selector:
        number:
          min: 1
          max: 120
          unit_of_measurement: min
          step: 1
          mode: slider
    use_dehumidifying_heating:
      name: Use Dehumidifying Heating
      description: Enable this to turn on heating if the humidity is too high.
      default: false
      selector:
        boolean: { }
    dehumification_temperature:
      name: Dehumification Temperature
      description: The temperature to set when the humidity is too high.
      default: 22
      selector:
        number:
          min: 5.0
          max: 30.0
          unit_of_measurement: °C
          step: 0.1
          mode: slider
    desired_humidity:
      name: Target Humidity
      description: The desired humidity level.
      default: 50
      selector:
        number:
          min: 0.0
          max: 100.0
          unit_of_measurement: "%"
          step: 1.0
          mode: slider
    actual_humidity_entity:
      name: Actual Humidity Entity
      description: The entity that holds the actual humidity.
      selector:
        entity:
          domain:
            - sensor
            - input_number
          multiple: false


mode: single

variables:
  max_temp_diff_for_manual_control: 0.2
  desired_temperature_change_delay: 1
  #####
  thermostat_entity: !input 'thermostat_entity'
  _actual_temperature_entity: !input 'actual_temperature_entity'
  actual_temperature: '{{ states(_actual_temperature_entity) | float }}'
  desired_temperature_entity: !input 'desired_temperature_entity'
  last_desired_temperature_change: '{{ desired_temperature_entity.last_changed }}'
  desired_temperature: '{{ states(desired_temperature_entity) | float }}'
  manual_control_duration: !input 'manual_control_duration'
  use_dehumidifying_heating: !input 'use_dehumidifying_heating'
  dehumification_temperature: !input 'dehumification_temperature'
  desired_humidity: !input 'desired_humidity'
  _actual_humidity_entity: !input 'actual_humidity_entity'
  actual_humidity: '{{ states(_actual_humidity_entity) | float }}'
  desired_thermostat_temperature: '{{ min(max(desired_temperature + (desired_temperature - actual_temperature) | float, 5.0), 30.0) | round(1) }}'
  current_thermostat_temperature: '{{ state_attr(thermostat_entity, "temperature") | float }}'
  # manual control is gained when the diff is greater than the max_temp_diff_for_manual_control and the desired temperature has not been changed for the desired_temperature_change_delay
  #  manual_control_active: '{{ (current_thermostat_temperature - desired_thermostat_temperature) | abs > max_temp_diff_for_manual_control
  #   and as_timestamp(now()) - as_timestamp(last_desired_temperature_change) > desired_temperature_change_delay }}'
  manual_control_active: '{{ (current_thermostat_temperature - desired_thermostat_temperature) | abs > max_temp_diff_for_manual_control
  and trigger.entity_id == thermostat_entity }}'
  timer_end_time: '{{ as_timestamp(now() + timedelta(minutes=manual_control_duration)) }}'



trigger:
  - platform: state
    entity_id: !input 'thermostat_entity'
  - platform: state
    entity_id: !input 'actual_temperature_entity'
  - platform: state
    entity_id: !input 'desired_temperature_entity'
  - platform: state
    entity_id: !input 'actual_humidity_entity'


action:
  - choose:
      - conditions:
          - condition: template
            value_template: "{{ manual_control_active }}"
            # also check, if trigger was the thermostat entity
        sequence:
          - service: persistent_notification.create
            data:
              # log all the variables and the trigger state, add their names to be able to identify them, also add newlines for better readability
              message: >
                Triggered by: {{ trigger.entity_id }}
                Variables:
                thermostat_entity: {{ thermostat_entity }}
                actual_temperature: {{ actual_temperature }}
                desired_temperature: {{ desired_temperature }}
                manual_control_duration: {{ manual_control_duration }}
                desired_thermostat_temperature: {{ desired_thermostat_temperature }}
                thermostat_temperature: {{ current_thermostat_temperature }}
                last_changed: {{ states[thermostat_entity].last_changed }}
                manual_control: {{ (current_thermostat_temperature - desired_thermostat_temperature) | abs > 0.2 }}
                now: {{ now() }}
                timer_end_time: {{ timer_end_time }}
                timer_ended: {{ as_timestamp(now()) > timer_end_time }}
          - service: persistent_notification.create
            data:
              message: "Manual control active for {{ thermostat_entity.split('.')[1] }} for {{ manual_control_duration }} minutes to {{ current_thermostat_temperature }}."
          - alias: "Manual temperature control active"
            repeat:
              while:
                - condition: template
                  value_template: "{{ as_timestamp(now()) < timer_end_time }}"
                - condition: template
                  value_template: "{{ state_attr(thermostat_entity, 'temperature') | float != 5.0 }}"
              sequence:
                - delay:
                    seconds: 5

          - service: climate.set_temperature
            target:
              entity_id: !input 'thermostat_entity'
            data:
              temperature: '{{ desired_thermostat_temperature }}'
          - service: persistent_notification.create
            data:
              message: "Manual control deactivated for {{ thermostat_entity.split('.')[1] }}."


      - conditions:
          - condition: template
            value_template: "{{ use_dehumidifying_heating }}"
          - condition: template
            value_template: >-
              {{ (actual_humidity | float) > (desired_humidity | float) }}
        sequence:
          - service: climate.set_temperature
            target:
              entity_id: !input 'thermostat_entity'
            data:
              temperature: "{{ dehumification_temperature + (dehumification_temperature - actual_temperature) | float }}"
    default:
      - service: climate.set_temperature
        target:
          entity_id: !input 'thermostat_entity'
        data:
          temperature: '{{ desired_thermostat_temperature }}'